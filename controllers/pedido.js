const moment = require('moment');
const util = require('util');

const Pedido = require('../models/Pedido');
const Produto = require('../models/Produto');

/*
* Cadastro de pedido.
*/
exports.cadastroPedido = (req, res, next) => {

	if(!req.body.codigo){
		throw new Error('É necessário informar o código do pedido.');
	}
	if(!req.body.nomeComprador){
		throw new Error('É necessário informar o nome do comprador do pedido.');
	}
	if(!req.body.estado){
		throw new Error('É necessário informar o estado do pedido.');
	}
	if(req.body.listaItem && req.body.listaItem.length === 0){
		throw new Error('Nenhum produto foi adicionado ao pedido.');
	}

	let pedido = new Pedido ({
		codigo: req.body.codigo,
		nomeComprador: req.body.nomeComprador,
		estado: req.body.estado,
		valorFrete: req.body.valorFrete,
		listaItem: req.body.listaItem
	});

	let estoque = pedido.listaItem.map((produto) => {

		return atualizarEstoque(produto.codigo, produto.quantidade, 'subtrair');
	});


	return Promise.all(estoque).then((produtos) => {

		if(!produtos) {
			throw new Error('Problema ao atualizar estoque.');
		}

		return pedido.save();
	}).then((pedido) => {

		if(!pedido){
			throw new Error('Problema ao criar pedido.');
		}

		//console.log(pedido)
		res.status(200).json(pedido);
	}).catch((err) => {
		
		return next(err);
	});
};

/*
	Atualiza o pedido pelo código.
*/
exports.atualizaPedido = (req, res, next) => {

	let pedidoAtualizado = req.body;
	pedidoAtualizado.codigo = req.params.codigo;

	let promisePedido;

	if(req.body.listaItem && req.body.listaItem.length > 0){

		promisePedido = compararItemPedido(pedidoAtualizado).then((pedido) => {

			return Pedido.findOneAndUpdate({ codigo : req.params.codigo }, req.body, {new:true});
		});
	} else {
		promisePedido = Pedido.findOneAndUpdate({ codigo : req.query.codigo }, req.body, {new: true});
	}

	return promisePedido.then((pedido) => {

		if(!pedido){
			throw new Error('Houve um erro na atualização do pedido.');
		}
		res.status(200).json(pedido);
	}).catch((err) => {

		return next(err);
	});	
};

/*
	Busca o pedido pelo código.
*/
exports.buscaPedido = (req, res, next) => {

	return Pedido.findOne(req.params).then((pedido) => {

		if(!pedido){
			throw new Error('Não existe um pedido com este código.');
		}

		let _pedido = {
			codigo: pedido.codigo,
			nomeComprador: pedido.nomeComprador,
			estado: pedido.estado,
			valorFrete: pedido.valorFrete,
			listaItem: pedido.listaItem,
			dataCompra: moment(pedido.dataCompra).format('DD/MM/YYYY HH:MM')
		}
		res.status(200).json(_pedido);
	}).catch((err) => {

		return next(err);
	});
}

/*
	Atualiza o estoque de produtos conforme o cadastro ou atualizção de pedidos.
*/
function atualizarEstoque (produtoPedido, quantidade, operacao) {
	
	//console.log(produtoPedido)
	return Produto.findOne({codigo: produtoPedido}).then((produto) => {

		if(!produto){
			throw new Error('O código do produto não existe.');
		}

		if(operacao === 'subtrair') {
			let quantEstoque = produto.estoque - quantidade;
			if(quantEstoque < 0) {
				//console.log('sem estoque')
				throw new Error('O estoque do produto ' + produto.nome + ' não é insuficiente.');
			}
			return Produto.updateOne({codigo: produto.codigo}, {estoque: quantEstoque});				
		}

		if(operacao === 'somar') {
			let quantEstoque = produto.estoque + quantidade;
			return Produto.updateOne({codigo: produto.codigo}, {estoque: quantEstoque});				
		}
	});
}

/*
	Verifica se houve mudança na lista de itens na atualização de um pedido.
*/
function compararItemPedido (pedidoAtualizado) {

	return Pedido.findOne({codigo: pedidoAtualizado.codigo}).then((pedidoAntigo) => {

		let produto;
		let diferenca = 0;
		let operacao = '';

		pedidoAtualizado.listaItem.forEach((produtoAtualizado) => {
			pedidoAntigo.listaItem.forEach((produtoAntigo) => {

				if (produtoAntigo.quantidade == produtoAtualizado.quantidade || produtoAntigo.codigo != produtoAtualizado.codigo) {
					return;
				}

				if (produtoAntigo.quantidade > produtoAtualizado.quantidade){
					//console.log('Diminui a quantidade')
					let diferenca = 0;
					diferenca = produtoAntigo.quantidade - produtoAtualizado.quantidade;
					operacao = 'subtrair';
				} else if (produtoAntigo.quantidade < produtoAtualizado.quantidade){
					//console.log('Aumentou a quantidade')
					let diferenca = 0;
					diferenca = produtoAtualizado.quantidade - produtoAntigo.quantidade;
					operacao = 'somar';
				} 
				return atualizarEstoque (produtoAtualizado.codigo, diferenca, operacao);
			});			
		});
	});
}