//const moment = require('moment');
const moment = require('moment-timezone');
const fs = require('fs');

const Pedido = require('../models/Pedido');
const Produto = require('../models/Produto');

/*
	Gera o relatório de pedidos de ticket médio.
	Um relatório detalhado é escrito em arquivo, enquanto um relatório resumido é enviado como resposta da requisição.
*/
exports.ticketMedio = (req, res, next) => {

	const dataInicio = moment(req.query.dataInicio, 'DD/MM/YYYY');
	//const dataInicio = moment.tz(req.query.dataInicio, 'America/Sao_Paulo');
	const dataFim = moment(req.query.dataFim, 'DD/MM/YYYY').set({hour:23,minute:59,second:59});
	//const dataFim = moment.tz(req.query.dataFim, 'America/Sao_Paulo');
	let quantidadeVendas = 0;
	let valorVendidoTotal = 0;
	let ticketMedio = 0;

	let relatorio = fs.createWriteStream('./relatorio/' + moment(dataInicio).format('DD-MM-YYYY') + '_' + moment(dataFim).format('DD-MM-YYYY') + '.txt', 'ascii');

	return Pedido.find({
		dataCompra: {
			$gte: dataInicio,
			$lte: dataFim
		}
	}).then((pedidos) => {

		if(pedidos && pedidos.length <= 0){
			throw new Error('Não existem pedidos nesse período.')
		}
		pedidos.map((pedido) => {
			relatorio.write("Código do pedido: " + pedido.codigo + '\n' +
				"Data da compra: " + moment(pedido.dataCompra).format('DD/MM/YYYY HH:MM') + '\n' +
				"Nome do comprador: " + pedido.nomeComprador + '\n' +
				"Estado do pedido: " + pedido.estado + '\n' +
				"Valor do frete: R$ " + pedido.valorFrete + '\n' +
				"Lista de item: " + '\n');
			valorVendidoTotal = valorVendidoTotal + pedido.valorFrete;
			pedido.listaItem.map((produto) => {

				relatorio.write("Código do produto: " + produto.codigo + '\n' +
					"Quantidade: " + produto.quantidade + "\n" +
					"Preço de venda: R$" + produto.preco + "\n");
				valorVendidoTotal = valorVendidoTotal + (produto.preco * produto.quantidade);
			});

			relatorio.write("---------------------------------------------------------- \n");
		});	

		relatorio.end();

		quantidadeVendas = pedidos.length;
		ticketMedio = valorVendidoTotal/quantidadeVendas;
		
		res.status(200).json({
			'dataInicio': req.query.dataInicio,
			'dataFinal': req.query.dataFim,
			'quantidadeVendas': quantidadeVendas,
			'valorTotalVendido': valorVendidoTotal,
			'valorTicketMedio': ticketMedio
		});
	}).catch((err) => {

		return next(err);
	});
}