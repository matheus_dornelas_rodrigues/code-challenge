const Produto = require('../models/Produto');
const util = require('util');

/*
	Cadastro de produtos.
*/
exports.cadastroProduto = (req, res, next) => {

	if(!req.body.codigo){
		throw new Error('É necessário informar um código para o produto.');
	}
	if(!req.body.nome){
		throw new Error('É necessário informar um nome para o produto.');
	}
	if(!req.body.preco){
		throw new Error('É necessário informar um preço para o produto.');
	}

	const produto = new Produto (req.body);

	return Produto.findOne({ codigo: req.body.codigo }).then((existingProduto) => {

		if(existingProduto){
			throw new Error('Já existe um produto com este código.');
		}

		return produto.save();
	}).then((produto) => {

		res.status(200).json(produto);
	}).catch((err) => {

		return next(err);
	});
};

/*
	Busca o produto pelo código.
*/
exports.buscaProduto = (req, res, next) => {

	if(!req.query.codigo && !req.query.nome){
		throw new Error('É necessário informar um código ou nome para realizar a busca.');
	}

	return Produto.findOne(req.query).then((produto) => {
		if(!produto){
			throw new Error('Não existe um produto com este nome.');
		}

		let prod = {
			codigo: produto.codigo,
			nome: produto.nome,
			estoque: produto.estoque,
			preco: produto.preco,
			caracteristica: produto.caracteristica
		}
		res.status(200).json(produto);
	}).catch((err) => {

		return next(err);
	});
}

/*
	Atualiza o produto pelo código.
*/
exports.atualizaProduto = (req, res, next) => {
	
	if(!req.params.codigo){
		throw new Error('É necessário informar um código para atualizar o produto.');
	}

	return Produto.findOneAndUpdate({codigo: req.params.codigo}, req.body, {new: true}).then((produto) => {
		if(!produto){
			throw new Error('Não existe um produto com este código.');
		}
		//console.log(produto)
		/*return produto.save();
		if(produto){
			produto.nome = req.body.nome || produto.nome;
			produto.descricao = req.body.descricao || produto.descricao;
			produto.estoque = req.body.estoque || produto.estoque;
			produto.preco = req.body.preco || produto.preco;
			produto.caracteristica = req.body.caracteristica || produto.caracteristica;
		*/
		/*produto.save((err) => {
			if(err){
				return next(err);
			}
*/
			res.status(200).json(produto);
//		});
		
	}).catch((err) => {

		return next(err);
	});
}

/*
	Remove o produto pelo código.
*/
exports.removeProduto = (req, res, next) => {

	if(!req.params.codigo){
		throw new Error('É necessário informar um código para remover o produto.');
	}

	return Produto.deleteOne({codigo: req.params.codigo}).then((result) => {
		
		if(result.deletedCount === 0){

			throw new Error('Não foi encontrado o produto com este código.');
		}

		if(result.ok ==! 1){
			
			throw new Error('Houve um problema na remoção do produto.');
		}

		res.status(200).json(result);
	}).catch((err) => {

		return next(err);
	});
}