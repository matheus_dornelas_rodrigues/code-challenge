const faker = require('faker/locale/pt_BR');
const util = require('util');
const server = require('../app');

const Produto = require('../models/Produto');
const Pedido = require('../models/Pedido');
const mongoose = require('mongoose');


exports.sampleData = (req, res, next) => {

	let promiseProdutos = criaProdutos(5);
	let promisoPedidos = criaPedidos(3);

	Promise.all([promiseProdutos, promisoPedidos]).then((_produtos) => {

		res.status(200).json(_produtos);
	}).catch((err) => {

		return next(err);
	})
}

function criaProdutos (quantidade) {

	let produtos = new Array(quantidade);

	for (let i = 0; i<produtos.length; i++) {

		let produto = {
			codigo: i+1,
			nome:faker.commerce.productName(),
			descricao: faker.commerce.productAdjective(),
			estoque: faker.random.number({
				min: 0,
				max: 1000
			}),
			preco: faker.commerce.price(),
			caracteristica:{
				cor: faker.commerce.color(),
				tipo: faker.commerce.productMaterial()
			}
		};

		produtos[i] = produto;
	}

	return produtos;
}

function criaPedidos (quantidade) {

	let pedidos = new Array(quantidade);

	for (let i = 0; i<pedidos.length; i++) {
	  
		let pedido = {
			codigo: i+1,
			nomeComprador: faker.name.firstName() + ' ' + faker.name.lastName(),
			dataCompra: faker.date.recent(6),
			estado: faker.random.arrayElement(['novo','aprovado','entregue','cancelado']),
			valorFrete: faker.commerce.price(),
			listaItem:[
				{
					codigo: faker.random.number({
						min: 1,
						max: 10
					}),
					quantidade: faker.random.number({
						min: 1,
						max: 100
					}),
					preco: faker.commerce.price()
				},
				{
					codigo: faker.random.number({
						min: 1,
						max: 10
					}),
					quantidade: faker.random.number({
						min: 1,
						max: 100
					}),
					preco: faker.commerce.price()
				}
			]
		};

		pedidos[i] = pedido;
	}
	
	return pedidos;
}