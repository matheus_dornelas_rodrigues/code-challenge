const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');

//Aponta o arquivo da variável de ambiente
dotenv.load({ path: '.env' });

const produtoController = require('./controllers/produto');
const pedidoController = require('./controllers/pedido');
const relatorioController = require('./controllers/relatorio');
const seed = require('./seed/seeds');
const util = require('util');

const app = express();


/*
	Configuração do MongoDB
*/
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useNewUrlParser', true);

mongoose.connect(process.env.MONGODB_URI);
mongoose.connection.on('error', (err) => {
  console.error(err);
  console.log('%s MongoDB connection error. Please make sure MongoDB is running.', chalk.red('✗'));
  process.exit();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

/*
	Registro das rotas de Produto.
*/
app.post('/produto/cadastro', produtoController.cadastroProduto);
app.get('/produto/busca', produtoController.buscaProduto);
app.put('/produto/atualiza/:codigo', produtoController.atualizaProduto);
app.delete('/produto/remove/:codigo', produtoController.removeProduto);

/*
	Registro das rotas de Pedido
*/
app.post('/pedido/cadastro', pedidoController.cadastroPedido);
app.get('/pedido/busca/:codigo', pedidoController.buscaPedido);
app.put('/pedido/atualiza/:codigo', pedidoController.atualizaPedido);

/*
	Registro das rotas de Relatório
*/
app.get('/relatorio/ticketmedio', relatorioController.ticketMedio);

/*
	Registro das rotas de sample data
*/
app.get('/sampledata', seed.sampleData);

/*
	Error Handler
*/
app.use((err, req, res, next) => {
	
	if (res.headersSent) {
		return next(err);
	}
	//console.log(err)
	res.status(500).send(err.message);
});

app.listen(3000, function () {
  console.log('API Code Challenge executando em localhost:3000!');
});

module.exports = app;