const mongoose = require('mongoose');

const produtoSchema = new mongoose.Schema({
  codigo: { type: Number, unique: true, required: true },
  nome: { type: String, required: true }, 
  descricao: String,
  estoque: { type: Number, default: 0, required: true },
  preco: { type: Number, required: true },
  caracteristica: mongoose.Schema.Types.Mixed
}, { timestamps: true });

const Produto = mongoose.model('Produto', produtoSchema);

module.exports = Produto;
