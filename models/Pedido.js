const mongoose = require('mongoose');

const pedidoSchema = new mongoose.Schema({
  codigo: { type: Number, unique: true, required: true },
  nomeComprador: {type: String, required: true },
  dataCompra: { type: Date, default: Date.now },
  estado: String,
  valorFrete: { type: Number, default: 0 },
  listaItem: { type: [{
  	codigo: Number,
  	quantidade: Number,
  	preco: Number
  }], required: true, min: 1 }
}, { timestamps: true });

const Pedido = mongoose.model('Pedido', pedidoSchema);

module.exports = Pedido;
