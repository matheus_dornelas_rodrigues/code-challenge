const mongoose = require("mongoose");
const Produto = require('../models/Produto');
const Pedido = require('../models/Pedido');

//Require a rota dev-dependencies
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');
const should = chai.should();
const { expect } = require('chai')
const util = require('util');

chai.use(chaiHttp);

describe('Produto', () => {
	it('Limpando banco de dados', (done) => {
        Produto.deleteMany({}, (err) => { 
           done();           
        });        
	});

	/*
	* Testa a rota /POST /produto/cadastro
	*/
	describe('/POST /produto/cadastro', (done) => {
		it('deve cadastrar o produto', (done) => {
			let produto = {
				codigo: 1,
				nome: "Frigideira",
				estoque: 150,
				preco: 30,
				caracteristica: {
					revestimento: 'teflon'
				}
			}
		chai.request(server)
			.post('/produto/cadastro')
			.send(produto)
			.end((err, res) => {
				res.should.have.status(200);
				res.body.should.be.a('object');
				res.body.should.have.property('codigo');
				res.body.should.have.property('nome');
				res.body.should.have.property('estoque');
				res.body.should.have.property('preco');
				res.body.should.have.property('caracteristica');
				res.body.caracteristica.should.be.a('object');
				done();
			});
		});
	});

	describe('/POST /produto/cadastro', (done) => {
		it('deve cadastrar o produto', (done) => {
			let produto = {
				codigo: 7,
				nome: "Pratos",
				descricao: 'Pratos grandes azuis e rasos.',
				estoque: 280,
				preco: 25.5,
				caracteristica: {
					material: 'vidro',
					cor: 'azul',
				}
			}
		chai.request(server)
			.post('/produto/cadastro')
			.send(produto)
			.end((err, res) => {
				res.should.have.status(200);
				res.body.should.be.a('object');
				res.body.should.have.property('codigo');
				res.body.should.have.property('nome');
				res.body.should.have.property('estoque');
				res.body.should.have.property('preco');
				res.body.should.have.property('caracteristica');
				res.body.caracteristica.should.be.a('object');
				done();
			});
		});
	});

	describe('/POST /produto/cadastro', (done) => {
		it('deve cadastrar o produto', (done) => {
			let produto = {
				codigo: 8,
				nome: "Liquidificador",
				descricao: 'Liquidificador Arno 220v',
				estoque: 100,
				preco: 120,
				caracteristica: {
					Marca: 'Arno',
					cor: 'branco',
					modelo: 'xxxx'
				}
			}
		chai.request(server)
			.post('/produto/cadastro')
			.send(produto)
			.end((err, res) => {
				res.should.have.status(200);
				res.body.should.be.a('object');
				res.body.should.have.property('codigo');
				res.body.should.have.property('nome');
				res.body.should.have.property('estoque');
				res.body.should.have.property('preco');
				res.body.should.have.property('caracteristica');
				res.body.caracteristica.should.be.a('object');
				done();
			});
		});
	});

	describe('/POST /produto/cadastro', (done) => {
	  it('não deve cadastrar produtos com códigos iguais', (done) => {
			let produto = {
				codigo: 1,
				nome: "Frigideira",
				estoque: 150,
				preco: 30,
				caracteristica: {
					revestimento: 'teflon'
				}
			}
		chai.request(server)
			.post('/produto/cadastro')
			.send(produto)
			.end((err, res) => {
				res.should.have.status(500);
				res.should.have.property('error');
				res.error.should.have.property('text');
				expect(res.error.text).to.be.equal('Já existe um produto com este código.');
				done();
			});
		});
	});

	describe('/POST /produto/cadastro', (done) => {
	  it('deve cadastrar o produto sem característica', (done) => {
			let produto = {
				codigo: 2,
				nome: "Panela",
				estoque: 150,
				preco: 80
			}
		chai.request(server)
			.post('/produto/cadastro')
			.send(produto)
			.end((err, res) => {
				res.should.have.status(200);
				res.body.should.be.a('object');
				res.body.should.have.property('codigo');
				res.body.should.have.property('nome');
				res.body.should.have.property('estoque');
				res.body.should.have.property('preco');
				expect(res.body).to.not.have.property('caracteristica');
				done();
			});
		});
	});

	describe('/POST /produto/cadastro', (done) => {
	  it('não deve cadastrar o produto sem codigo', (done) => {
			let produto = {
				nome: "Garfos",
				estoque: 1000,
				preco: 5
			}
		chai.request(server)
			.post('/produto/cadastro')
			.send(produto)
			.end((err, res) => {
				res.should.have.status(500);
				res.should.have.property('error');
				res.error.should.have.property('text');
				//console.log(util.inspect(res.error, {showHidden: false, depth: null}));
				expect(res.error.text).to.be.equal('É necessário informar um código para o produto.');
				done();
			});
		});
	});

	describe('/POST /produto/cadastro', (done) => {
	  it('não deve cadastrar o produto sem nome', (done) => {
			let produto = {
				codigo: 4,
				estoque: 1000,
				preco: 5
			}
		chai.request(server)
			.post('/produto/cadastro')
			.send(produto)
			.end((err, res) => {
				res.should.have.status(500);
				res.should.have.property('error');
				res.error.should.have.property('text');
				expect(res.error.text).to.be.equal('É necessário informar um nome para o produto.');
				done();
			});
		});
	});

	describe('/POST /produto/cadastro', (done) => {
	  it('não deve cadastrar o produto sem preco', (done) => {
			let produto = {
				codigo: 4,
				nome: 'Garfos',
				estoque: 1000
			}
		chai.request(server)
			.post('/produto/cadastro')
			.send(produto)
			.end((err, res) => {
				res.should.have.status(500);
				res.should.have.property('error');
				res.error.should.have.property('text');
				expect(res.error.text).to.be.equal('É necessário informar um preço para o produto.');
				done();
			});
		});
	});

	describe('/POST /produto/cadastro', (done) => {
	  it('deve cadastrar o produto sem estoque', (done) => {
			let produto = {
				codigo: 4,
				nome: 'Garfos',
				preco: 5,
				caracteristica: {
					cor: "vermelho"
				}
			}
		chai.request(server)
			.post('/produto/cadastro')
			.send(produto)
			.end((err, res) => {
				res.should.have.status(200);
				res.body.should.be.a('object');
				res.body.should.have.property('codigo');
				res.body.should.have.property('nome');
				res.body.should.have.property('estoque');
				res.body.should.have.property('preco');
				res.body.should.have.property('caracteristica');
				res.body.caracteristica.should.be.a('object');
				done();
			});
		});
	});

	/*
	* Testa a rota /GET /produto/busca
	*/
	describe('/GET /produto/busca', () => {
		it('deve buscar um produto por código', (done) => {
		chai.request(server)
			.get('/produto/busca?codigo=1')
			.end((err, res) => {
				res.should.have.status(200);
				res.body.should.be.a('object');
				res.body.should.have.property('codigo');
				res.body.should.have.property('nome');
				res.body.should.have.property('estoque');
				res.body.should.have.property('preco');
				res.body.should.have.property('caracteristica');
				res.body.caracteristica.should.be.a('object');
			  done();
			});
		});
	});

	describe('/GET /produto/busca', () => {
		it('deve buscar um produto por nome', (done) => {
		chai.request(server)
			.get('/produto/busca?nome=Garfos')
			.end((err, res) => {
				res.should.have.status(200);
				res.body.should.be.a('object');
				res.body.should.have.property('codigo');
				res.body.should.have.property('nome');
				res.body.should.have.property('estoque');
				res.body.should.have.property('preco');
				res.body.should.have.property('caracteristica');
				res.body.caracteristica.should.be.a('object');
			  done();
			});
		});
	});

	describe('/GET /produto/busca', () => {
		it('não deve buscar um produto por código errado/inexistente', (done) => {
		chai.request(server)
			.get('/produto/busca?codigo=55')
			.end((err, res) => {
				res.should.have.status(500);
				res.should.have.property('error');
				res.error.should.have.property('text');
				expect(res.error.text).to.be.equal('Não existe um produto com este nome.');
			  done();
			});
		});
	});

	/*
	* Testa a rota /PUT /produto/atualiza/:codigo
	*/
	describe('/PUT /produto/atualiza/:codigo', (done) => {
	  it('deve atualizar todo produto', (done) => {
			let produto = {
				codigo: 6,
				nome: "Facas",
				descricao: 'Faca de aço inox serrilhada para uso comum.',
				estoque: 1000,
				preco: 5,
				caracteristica: {
					cor: 'vermelho',
					tipo: 'serrilhada',
					marca: 'Tramontina'
				}
			}
		chai.request(server)
			.put('/produto/atualiza/1')
			.send(produto)
			.end((err, res) => {
				//console.log(util.inspect(res.body, {showHidden: false, depth: null}));
				res.should.have.status(200);
				res.body.should.be.a('object');
				res.body.should.have.property('codigo');
				res.body.should.have.property('nome');
				res.body.should.have.property('estoque');
				res.body.should.have.property('preco');
				res.body.should.have.property('caracteristica');
				res.body.caracteristica.should.be.a('object');
				res.body.caracteristica.should.have.property('cor');
				res.body.caracteristica.should.have.property('tipo');
				res.body.caracteristica.should.have.property('marca');
				done();
			});
		});
	});

	describe('/PUT /produto/atualiza/:codigo', (done) => {
		it('deve atualizar apenas o nome do produto', (done) => {
			let produto = {
				nome: 'Bicicleta'
			}
		chai.request(server)
			.put('/produto/atualiza/6')
			.send(produto)
			.end((err, res) => {
				//console.log(util.inspect(res.body, {showHidden: false, depth: null}));
				res.should.have.status(200);
				res.body.should.be.a('object');
				res.body.should.have.property('codigo');
				res.body.should.have.property('nome');
				res.body.should.have.property('estoque');
				//expect(res.body.estoque).to.equal(1000);
				res.body.should.have.property('preco');
				res.body.should.have.property('caracteristica');
				res.body.caracteristica.should.be.a('object');
				res.body.caracteristica.should.have.property('cor');
				res.body.caracteristica.should.have.property('tipo');
				res.body.caracteristica.should.have.property('marca');
				done();
			});
		});
	});

	describe('/PUT /produto/atualiza/:codigo', (done) => {
		it('deve atualizar apenas a descrição do produto', (done) => {
			let produto = {
				descricao: 'Bicicleta de 21 marchas, aro 21, para adultos.'
			}
		chai.request(server)
			.put('/produto/atualiza/6')
			.send(produto)
			.end((err, res) => {
				//console.log(util.inspect(res.body, {showHidden: false, depth: null}));
				res.should.have.status(200);
				res.body.should.be.a('object');
				res.body.should.have.property('codigo');
				res.body.should.have.property('nome');
				res.body.should.have.property('estoque');
				//expect(res.body.estoque).to.equal(1000);
				res.body.should.have.property('preco');
				res.body.should.have.property('caracteristica');
				res.body.caracteristica.should.be.a('object');
				res.body.caracteristica.should.have.property('cor');
				res.body.caracteristica.should.have.property('tipo');
				res.body.caracteristica.should.have.property('marca');
				done();
			});
		});
	});

	describe('/PUT /produto/atualiza/:codigo', (done) => {
		it('deve atualizar apenas o estoque do produto', (done) => {
			let produto = {
				estoque: 1000
			}
		chai.request(server)
			.put('/produto/atualiza/4')
			.send(produto)
			.end((err, res) => {
				//console.log(util.inspect(res.body, {showHidden: false, depth: null}));
				res.should.have.status(200);
				res.body.should.be.a('object');
				res.body.should.have.property('codigo');
				res.body.should.have.property('nome');
				res.body.should.have.property('estoque');
				expect(res.body.estoque).to.equal(1000);
				res.body.should.have.property('preco');
				res.body.should.have.property('caracteristica');
				res.body.caracteristica.should.be.a('object');
				res.body.caracteristica.should.have.property('cor');
				done();
			});
		});
	});

	describe('/PUT /produto/atualiza/:codigo', (done) => {
		it('deve atualizar apenas o preço do produto', (done) => {
			let produto = {
				preco: 849.49
			}
		chai.request(server)
			.put('/produto/atualiza/6')
			.send(produto)
			.end((err, res) => {
				//console.log(util.inspect(res.body, {showHidden: false, depth: null}));
				res.should.have.status(200);
				res.body.should.be.a('object');
				res.body.should.have.property('codigo');
				res.body.should.have.property('nome');
				res.body.should.have.property('estoque');
				//expect(res.body.estoque).to.equal(1000);
				res.body.should.have.property('preco');
				res.body.should.have.property('caracteristica');
				res.body.caracteristica.should.be.a('object');
				res.body.caracteristica.should.have.property('cor');
				res.body.caracteristica.should.have.property('tipo');
				res.body.caracteristica.should.have.property('marca');
				done();
			});
		});
	});

	describe('/PUT /produto/atualiza/:codigo', (done) => {
		it('deve atualizar apenas as característica do produto', (done) => {
			let produto = {
				caracteristica: {
					marchas: 21,
					cor: 'azul',
					aro: 24
				}
			}
		chai.request(server)
			.put('/produto/atualiza/6')
			.send(produto)
			.end((err, res) => {
				//console.log(util.inspect(res.body, {showHidden: false, depth: null}));
				res.should.have.status(200);
				res.body.should.be.a('object');
				res.body.should.have.property('codigo');
				res.body.should.have.property('nome');
				res.body.should.have.property('estoque');
				//expect(res.body.estoque).to.equal(1000);
				res.body.should.have.property('preco');
				res.body.should.have.property('caracteristica');
				res.body.caracteristica.should.be.a('object');
				res.body.caracteristica.should.have.property('marchas');
				res.body.caracteristica.should.have.property('cor');
				res.body.caracteristica.should.have.property('aro');
				done();
			});
		});
	});

	describe('/PUT /produto/atualiza/:codigo', (done) => {
		it('não deve atualizar o produto a partir de um código inexistente', (done) => {

		chai.request(server)
			.put('/produto/atualiza/13')
			.end((err, res) => {
				res.should.have.status(500);
				res.should.be.a('object');
				res.should.have.property('error');
				res.error.should.have.property('text');
				expect(res.error.text).to.be.equal('Não existe um produto com este código.');
				done();
			});
		});
	});

	describe('/PUT /produto/atualiza/:codigo', (done) => {
		it('não deve atualizar o produto se o código não for informado', (done) => {

		chai.request(server)
			.put('/produto/atualiza')
			.end((err, res) => {
				res.should.have.status(404);
				res.should.be.a('object');
				res.should.have.property('error');
				res.error.should.have.property('text');
				//expect(res.error.text).to.be.equal('É necessário informar um código para atualizar o produto.');
				done();
			});
		});
	});

	/*
	* Testa a rota /DELETE /produto/remove/:codigo
	*/
	describe('/DELETE /produto/remove/:codigo', (done) => {
	  it('deve apagar o produto a partir do código', (done) => {
		chai.request(server)
			.delete('/produto/remove/6')
			.end((err, res) => {
				res.should.have.status(200);
				res.body.should.be.a('object');
				res.body.should.have.property('deletedCount');
				expect(res.body.deletedCount).to.be.equal(1);
				res.body.should.have.property('deletedCount');
				expect(res.body.ok).to.be.equal(1);
				done();
			});
		});
	});

	describe('/DELETE /produto/remove/:codigo', (done) => {
	  it('não deve apagar o produto a partir de um código inexistente', (done) => {
		chai.request(server)
			.delete('/produto/remove/55')
			.end((err, res) => {
				res.should.have.status(500);
				res.should.be.a('object');
				res.should.have.property('error');
				res.error.should.have.property('text');
				expect(res.error.text).to.be.equal('Não foi encontrado o produto com este código.');
				done();
			});
		});
	});

	describe('/DELETE /produto/remove/:codigo', (done) => {
	  it('não deve apagar o produto se o código não for informado', (done) => {
		chai.request(server)
			.delete('/produto/remove')
			.end((err, res) => {
				res.should.have.status(404);
				res.should.be.a('object');
				res.should.have.property('error');
				res.error.should.have.property('text');
				//console.log(util.inspect(res.error, {showHidden: false, depth: null}));
				//expect(res.error.text).to.be.equal('É necessário informar um código para remover o produto.');
				done();
			});
		});
	});
});


describe('Pedido', () => {

	it('Limpando banco de dados', (done) => {
        Pedido.deleteMany({}, (err) => { 
           done();           
        });        
	});

	/*
	* Testa a rota /POST /pedido/cadastro
	*/
	describe('/POST /pedido/cadastro', (done) => {
	  it('deve cadastrar o pedido', (done) => {
			let pedido = {
				codigo: 1,
				nomeComprador: 'Matheus Dornelas',
				estado: 'novo',
				valorFrete: 40,
				listaItem: [
				{
					codigo: 2,
					quantidade: 10,
					preco: 50
				},
				{
					codigo: 4,
					quantidade: 1000,
					preco: 60
				}]
			}
		chai.request(server)
			.post('/pedido/cadastro')
			.send(pedido)
			.end((err, res) => {
				//console.log(util.inspect(err, {showHidden: false, depth: null}));				
				res.should.have.status(200);
				res.body.should.be.a('object');
				res.body.should.have.property('codigo');
				res.body.should.have.property('nomeComprador');
				res.body.should.have.property('estado');
				res.body.should.have.property('valorFrete');
				res.body.should.have.property('listaItem');
				res.body.listaItem.should.be.a('array');
				expect(res.body.listaItem).to.have.lengthOf(2);
				expect(res.body.listaItem[0]).to.have.property('codigo');
				expect(res.body.listaItem[0]).to.have.property('quantidade');
				expect(res.body.listaItem[0]).to.have.property('preco');
				done();
			});
		});
	});

	describe('/POST /pedido/cadastro', (done) => {
	  it('deve cadastrar o pedido', (done) => {
			let pedido = {
				codigo: 2,
				nomeComprador: 'AAAAA BBBBB',
				estado: 'finalizado',
				valorFrete: 90,
				listaItem: [
				{
					codigo: 8,
					quantidade: 50,
					preco: 120
				}]
			}
		chai.request(server)
			.post('/pedido/cadastro')
			.send(pedido)
			.end((err, res) => {
				//console.log(util.inspect(err, {showHidden: false, depth: null}));				
				res.should.have.status(200);
				res.body.should.be.a('object');
				res.body.should.have.property('codigo');
				res.body.should.have.property('nomeComprador');
				res.body.should.have.property('estado');
				res.body.should.have.property('valorFrete');
				res.body.should.have.property('listaItem');
				res.body.listaItem.should.be.a('array');
				expect(res.body.listaItem).to.have.lengthOf(1);
				expect(res.body.listaItem[0]).to.have.property('codigo');
				expect(res.body.listaItem[0]).to.have.property('quantidade');
				expect(res.body.listaItem[0]).to.have.property('preco');
				done();
			});
		});
	});

	describe('/POST /pedido/cadastro', (done) => {
	  it('deve cadastrar o pedido', (done) => {
			let pedido = {
				codigo: 3,
				nomeComprador: 'CCCCCCCCC DDDDDDD',
				estado: 'aberto',
				valorFrete: 0,
				listaItem: [
				{
					codigo: 8,
					quantidade: 50,
					preco: 120
				},
				{
					codigo: 2,
					quantidade: 20,
					preco: 50
				}]
			}
		chai.request(server)
			.post('/pedido/cadastro')
			.send(pedido)
			.end((err, res) => {
				//console.log(util.inspect(err, {showHidden: false, depth: null}));				
				res.should.have.status(200);
				res.body.should.be.a('object');
				res.body.should.have.property('codigo');
				res.body.should.have.property('nomeComprador');
				res.body.should.have.property('estado');
				res.body.should.have.property('valorFrete');
				res.body.should.have.property('listaItem');
				res.body.listaItem.should.be.a('array');
				expect(res.body.listaItem).to.have.lengthOf(2);
				expect(res.body.listaItem[0]).to.have.property('codigo');
				expect(res.body.listaItem[0]).to.have.property('quantidade');
				expect(res.body.listaItem[0]).to.have.property('preco');
				done();
			});
		});
	});

	describe('/POST /pedido/cadastro', (done) => {
	  it('não deve cadastrar o pedido sem código', (done) => {
			let pedido = {
				nomeComprador: 'Fulando Siclano',
				estado: 'finalizado',
				valorFrete: 23,
				listaItem: [
				{
					codigo: 10,
					quantidade: 10,
					preco: 50
				},
				{
					codigo: 4,
					quantidade: 15,
					preco: 60
				}]
			}
		chai.request(server)
			.post('/pedido/cadastro')
			.send(pedido)
			.end((err, res) => {
				//console.log(util.inspect(res.body, {showHidden: false, depth: null}));				
				res.should.have.status(500);
				res.should.be.a('object');
				res.should.have.property('error');
				res.error.should.have.property('text');
				expect(res.error.text).to.be.equal('É necessário informar o código do pedido.');
				done();
			});
		});
	});

	describe('/POST /pedido/cadastro', (done) => {
	  it('não deve cadastrar o pedido sem nome de comprador', (done) => {
			let pedido = {
				codigo: 2,
				estado: 'finalizado',
				valorFrete: 23,
				listaItem: [
				{
					codigo: 10,
					quantidade: 10,
					preco: 50
				},
				{
					codigo: 4,
					quantidade: 15,
					preco: 60
				}]
			}
		chai.request(server)
			.post('/pedido/cadastro')
			.send(pedido)
			.end((err, res) => {
				//console.log(util.inspect(res.body, {showHidden: false, depth: null}));				
				res.should.have.status(500);
				res.should.be.a('object');
				res.should.have.property('error');
				res.error.should.have.property('text');
				expect(res.error.text).to.be.equal('É necessário informar o nome do comprador do pedido.');
				done();
			});
		});
	});

	describe('/POST /pedido/cadastro', (done) => {
	  it('não deve cadastrar o pedido sem estado do pedido', (done) => {
			let pedido = {
				codigo: 2,
				nomeComprador: 'Fulando Siclano',
				valorFrete: 23,
				listaItem: [
				{
					codigo: 10,
					quantidade: 10,
					preco: 50
				},
				{
					codigo: 4,
					quantidade: 15,
					preco: 60
				}]
			}
		chai.request(server)
			.post('/pedido/cadastro')
			.send(pedido)
			.end((err, res) => {
				//console.log(util.inspect(res.body, {showHidden: false, depth: null}));				
				res.should.have.status(500);
				res.should.be.a('object');
				res.should.have.property('error');
				res.error.should.have.property('text');
				expect(res.error.text).to.be.equal('É necessário informar o estado do pedido.');
				done();
			});
		});
	});

	describe('/POST /pedido/cadastro', (done) => {
	  it('não deve cadastrar o pedido com código de produto inexistente', (done) => {
			let pedido = {
				codigo: 2,
				nomeComprador: 'Fulando Siclano',
				estado: 'finalizado',
				valorFrete: 23,
				listaItem: [
				{
					codigo: 10,
					quantidade: 10,
					preco: 50
				}]
			}
		chai.request(server)
			.post('/pedido/cadastro')
			.send(pedido)
			.end((err, res) => {
				//console.log(util.inspect(res.body, {showHidden: false, depth: null}));				
				res.should.have.status(500);
				res.should.be.a('object');
				res.should.have.property('error');
				res.error.should.have.property('text');
				expect(res.error.text).to.be.equal('O código do produto não existe.');
				done();
			});
		});
	});

	describe('/POST /pedido/cadastro', (done) => {
	  it('não deve cadastrar o pedido quando o estoque é insuficiente', (done) => {
			let pedido = {
				codigo: 3,
				nomeComprador: 'Fulando Siclano',
				estado: 'finalizado',
				valorFrete: 23,
				listaItem: [
				{
					codigo: 2,
					quantidade: 10,
					preco: 50
				},
				{
					codigo: 4,
					quantidade: 1,
					preco: 60
				}]
			}
		chai.request(server)
			.post('/pedido/cadastro')
			.send(pedido)
			.end((err, res) => {
				//console.log(util.inspect(res.body, {showHidden: false, depth: null}));				
				res.should.have.status(500);
				res.should.be.a('object');
				res.should.have.property('error');
				res.error.should.have.property('text');
				//expect(res.error.text).to.be.equal('O estoque do produto ' + .nome + ' não é insuficiente.');
				done();
			});
		});
	});

	/*
	* Testa a rota /GET /pedido/busca/:codigo
	*/
	describe('/GET /pedido/busca/:codigo', (done) => {
	  it('deve buscar o pedido a partir do código', (done) => {

		chai.request(server)
			.get('/pedido/busca/1')
			.end((err, res) => {
				res.should.have.status(200);
				res.body.should.be.a('object');
				res.body.should.have.property('codigo');
				res.body.should.have.property('nomeComprador');
				res.body.should.have.property('estado');
				res.body.should.have.property('valorFrete');
				res.body.should.have.property('dataCompra');
				res.body.should.have.property('listaItem');
				res.body.listaItem.should.be.a('array');
				expect(res.body.listaItem).to.have.lengthOf(2);
				expect(res.body.listaItem[0]).to.have.property('codigo');
				expect(res.body.listaItem[0]).to.have.property('quantidade');
				expect(res.body.listaItem[0]).to.have.property('preco');
				done();
			});
		});
	});

	describe('/GET /pedido/busca/:codigo', (done) => {
	  it('não deve buscar o pedido com código inexistente', (done) => {

		chai.request(server)
			.get('/pedido/busca/4')
			.end((err, res) => {
				res.should.have.status(500);
				res.should.be.a('object');
				res.should.have.property('error');
				res.error.should.have.property('text');
				expect(res.error.text).to.be.equal('Não existe um pedido com este código.');
				done();
			});
		});
	});

	describe('/PUT /pedido/atualiza', (done) => {
	  it('não deve atualizar o pedido se o código não for informado', (done) => {
	  	let pedido = {
	  		nomeComprador: "Fulano Siclano",
	  		valorFrete: 102,
	  		listaItem: [{
	  			codigo: 2,
	  			quantidade: 20,
	  			preco: 50
	  		}]
	  	}
		chai.request(server)
			.put('/pedido/atualiza')
			.send(pedido)
			.end((err, res) => {
				res.should.have.status(404);
				res.should.be.a('object');
				res.should.have.property('error');
				res.error.should.have.property('text');
				//expect(res.error.text).to.be.equal('É necessário informar um código para atualizar o pedido.');
				done();
			});
		});
	});
});

describe('Relatório', () => {

	/*
	* Testa a rota /GET /relatorio/ticketmedio
	*/
	describe('/GET /relatorio/ticketmedio', (done) => {
	  it('deve gerar um relatório com ticket médio', (done) => {

		chai.request(server)
			.get('/relatorio/ticketmedio')
			.query({dataInicio: '20/02/2019', dataFim: '26/02/2019'})
			.end((err, res) => {
				res.should.have.status(200);
				res.body.should.be.a('object');
				res.body.should.have.property('dataInicio');
				res.body.should.have.property('dataFinal');
				res.body.should.have.property('quantidadeVendas');
				res.body.should.have.property('valorTotalVendido');
				res.body.should.have.property('valorTicketMedio');
				done();
			});
		});
	});
});